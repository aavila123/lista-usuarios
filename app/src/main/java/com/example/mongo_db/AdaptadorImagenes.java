package com.example.mongo_db;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mongo_db.rest_1.model.Post;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdaptadorImagenes extends RecyclerView.Adapter<AdaptadorImagenes.ViewHolderImagenes> {

    ArrayList<Post> listaImagenes;

    public AdaptadorImagenes(ArrayList<Post> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    @NonNull
    @Override
    public AdaptadorImagenes.ViewHolderImagenes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderImagenes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorImagenes.ViewHolderImagenes holder, int position) {

        Picasso.get().load(listaImagenes.get(position).getUrlImage()).into(holder.imgLista);

        holder.descripcionLista.setText(listaImagenes.get(position).getDescription());


    }

    @Override
    public int getItemCount() {
        return listaImagenes.size();
    }

    public class ViewHolderImagenes extends RecyclerView.ViewHolder {

        ImageView imgLista;
        TextView descripcionLista;

        public ViewHolderImagenes(@NonNull View itemView) {
            super(itemView);

            imgLista = (ImageView)itemView.findViewById(R.id.imgLista);
            descripcionLista = (TextView)itemView.findViewById(R.id.descripcionLista);
        }
    }
}

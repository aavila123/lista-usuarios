package com.example.mongo_db.rest_1.adapter;

import com.example.mongo_db.rest_1.constants.ApiConstants;
import com.example.mongo_db.rest_1.model.Post;
import com.example.mongo_db.rest_1.service.MarketService;

import java.util.List;

import retrofit2.Call;

public class MarketAdapter extends BaseAdapter implements MarketService{

    private MarketService marketService;

    public MarketAdapter(){
        super(ApiConstants.BASE_POST_URL);
        marketService=createService(MarketService.class);
    }

    @Override
    public Call<Post> InsertPost(Post post) {
        return marketService.InsertPost(post);
    }

    @Override
    public Call<List<Post>> getPost() {
        return marketService.getPost();
    }




}

package com.example.mongo_db.rest_1.constants;

public class ApiConstants {

    public static final String BASE_POST_URL = "https://backend-posts.herokuapp.com/";

    public static final String MARKET_POST_ENDPOINT = "posts/";
}

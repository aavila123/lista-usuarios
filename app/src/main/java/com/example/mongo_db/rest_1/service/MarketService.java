package com.example.mongo_db.rest_1.service;

import com.example.mongo_db.rest_1.constants.ApiConstants;
import com.example.mongo_db.rest_1.model.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MarketService {

    @POST(ApiConstants.MARKET_POST_ENDPOINT)
    Call<Post> InsertPost(@Body Post post);

    @GET(ApiConstants.MARKET_POST_ENDPOINT)
    Call<List<Post>> getPost();
}

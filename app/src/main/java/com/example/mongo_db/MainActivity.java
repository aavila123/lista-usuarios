package com.example.mongo_db;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.mongo_db.rest_1.adapter.MarketAdapter;
import com.example.mongo_db.rest_1.model.Post;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText editTexttitulo, editTextDescriptcion;
    Button buttonPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        editTexttitulo = findViewById(R.id.TextTitle);
        editTextDescriptcion = findViewById(R.id.TextDescription);
        buttonPost = findViewById(R.id.ButtonPost);

        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PostPosts();
                Intent i = new Intent(MainActivity.this, activity_recycler.class);
                startActivity(i);
            }
        });
    }


    private void PostPosts() {
        MarketAdapter adapter = new MarketAdapter();
        Call<Post> call = adapter.InsertPost(new Post(editTexttitulo.getText().toString(), editTextDescriptcion.getText().toString(),
                        "https://media.metrolatam.com/2019/12/30/babyyoda3-9ba885040c874b6efb2c0f8875391df6-600x400.jpg"
                )
        );
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                Log.e("response", response.body().toString());
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }
}
